const url = 'http://localhost:3001/people';

async function getInfo () {
    return await fetch(url).then(data => data.json());
}

async function updateInfo (id, info) {
    return await fetch(`${url}/${id}`, {
        method: 'PUT',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify(info)
      }).then(data => data.json())
}

async function removePerson (id) {
    return await fetch(`${url}/${id}`, {
        method: 'DELETE',
        headers: {
          'Content-type': 'application/json'
        }
      }).then(data => data.json());
}

async function addNewPerson (info) {
    return await fetch(url, {
      method: 'POST',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify(info)
    }).then(data => data.json());
}

const functions = {
    getInfo,
    updateInfo,
    removePerson,
    addNewPerson
}

export default functions;
