import React, { useEffect, useState } from 'react'
import './App.css';
import Header from '../components/sidebar';
import Table from '../components/table';
import RegisterModal from '../components/register-modal';
import apiCalls from '../api/apiFunctions';

function App() {

  const [people, setPeople] = useState({});
  const [modalIsOpen, setModalIsOpen] = useState(false);

  useEffect(() => {
    getInfo();
  }, []);

  const toggleModal = () => {
    setModalIsOpen(!modalIsOpen);
  }

  const calculateId = () => {
    let id = people[people.length - 1].id + 1;
    return id;
  }

  const getInfo = () => {
    apiCalls.getInfo().then(data => setPeople(data));
  }

  const updateInfo = (id, info) => {
    apiCalls.updateInfo(id, info).then(data => {
        alert(`User ${id} successfully updated!`);
        getInfo();
    })
  }

  const removePerson = (id) => {
    apiCalls.removePerson(id).then(d => {
        alert(`User successfully deleted!`);
        getInfo();
      });
  }

  const addNewPerson = async (info) => {
    const addId = { ...info, id: calculateId() };

    await apiCalls.addNewPerson(addId).then(d => {
      alert('Person successfuly added!');
      toggleModal();
      getInfo();
    });
  }


  return (
      <div className="App">
        <Header openModal={toggleModal} />
        <div className="body">
          <RegisterModal
            isOpen={modalIsOpen}
            toggleModal={toggleModal} 
            addNewPerson={addNewPerson} 
          />
          <Table 
            data={people} 
            update={updateInfo} 
            remove={removePerson} 
          />
        </div>
      </div>
  );
}

export default App;
