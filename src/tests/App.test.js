import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';

import App from '../views/App';
import Table from '../components/table';
import mock from './mockup.json';
import RegisterModal from '../components/register-modal';

let app, registerButton;

const info = {
  name: 'Jessie',
  prof: 'Driver',
  email: 'jsc@ac.com',
  phone: '491238765678',
  country: 'Germany'
};

beforeEach(() => {
  app = render(<App />);
  registerButton = app.getByTestId('register-button');
});

describe('rendering', () => {
  
  test('renders app properly', () => {
    expect(app).toMatchSnapshot();
  });

  test('render register modal', () => {
    fireEvent.click(registerButton);
    const registerModal = screen.getByTestId('register-modal');

    expect(registerModal).toBeInTheDocument();
  });

  test('render mocked table informations', async () => {
    const table = render(<Table data={mock.people} />);

    expect(table).toMatchSnapshot();
  })

});

describe('insert data', () => {
  let inputName, inputProf, inputEmail, inputPhone, inputCountry;
  let people = mock.people;

  beforeEach(() => {
    render(<RegisterModal 
              isOpen 
              addNewPerson={() => people.push(info)} 
              toggleModal={() => 'modal toggled'} 
            />)

    inputName = screen.getByTestId('name-input');
    inputProf = screen.getByTestId('profession-input');
    inputEmail = screen.getByTestId('email-input');
    inputPhone = screen.getByTestId('phone-input');
    inputCountry = screen.getByTestId('country-input');

    fireEvent.change(inputName, { target: { value: info.name}});
    fireEvent.change(inputProf, { target: { value: info.prof}});
    fireEvent.change(inputEmail, { target: { value: info.email}});
    fireEvent.change(inputPhone, { target: { value: info.phone}});
    fireEvent.change(inputCountry, { target: { value: info.country}});
  });

  test('insert information on register modal', () => {

    const filledInputs = {
      name: inputName.value,
      prof: inputProf.value,
      email: inputEmail.value,
      phone: inputPhone.value,
      country: inputCountry.value
    }

    expect(filledInputs).toStrictEqual(info);

  })

  test('insert information on table', () => {
    const submitButton = screen.getByTestId('submit-button');
    fireEvent.click(submitButton);

    expect(people).toStrictEqual(mock.people);
  })
})

describe('manipulate inserted data on table', () => {

  let toggleEditButton, removeButton;
  const update = jest.fn();
  const remove = jest.fn();

  beforeEach(() => {

    render(<Table data={mock.people} update={update} remove={remove} />);

    toggleEditButton = screen.getAllByTestId('toggle-edit-button')[0];
    removeButton = screen.getAllByTestId('remove-button')[0];
  })
  
  test('edit information on table', () => {
    fireEvent.click(toggleEditButton);

    const nameField = screen.getByTestId("name-field");
    const updateButton = screen.getByTestId("update-button");

    fireEvent.change(nameField, { target: { value: 'newName' }});
    fireEvent.click(updateButton);

    expect(update).toHaveBeenCalled();

  })

  test('remove information on table', () => {
    fireEvent.click(removeButton);
    expect(remove).toHaveBeenCalled();
  })

})
