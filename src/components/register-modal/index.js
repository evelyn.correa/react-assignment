import React, { useState } from 'react';
import './register-modal.css'

const RegisterModal = ({ isOpen, addNewPerson, toggleModal }) => {
    const initialState = {
        name: "",
        profession: "",
        email: "",
        phone: "",
        country: ""
    }

    const [state, setState] = useState(initialState);

    const handleChange = e => {
        const { name, value } = e.target;
        setState(prevState => ({ ...prevState, [name]: value }));
    }

    const cleanModal = () => {
        setState(initialState);
        toggleModal();
    }

    const onSubmit = () => {
        addNewPerson(state);
        cleanModal();
    }

    return (isOpen &&
    <div className="modal" data-testid="register-modal">
        <article>
            <header className="modal-header">
                <h3>register new person</h3>
                <label onClick={toggleModal} className="close">&times;</label>
            </header>
            <section className="modal-content">
                <input 
                    placeholder="nome"
                    value={state.name}
                    name="name"
                    onChange={handleChange}
                    data-testid="name-input"
                />
                <input 
                    type="text" 
                    placeholder="profession"
                    value={state.profession}
                    name="profession"
                    onChange={handleChange}
                    data-testid="profession-input"
                />
                <input 
                    type="email" 
                    placeholder="e-mail"
                    value={state.email}
                    name="email"
                    onChange={handleChange}
                    data-testid="email-input"
                />
                <input 
                    type="tel" 
                    placeholder="phone number"
                    value={state.phone}
                    name="phone"
                    onChange={handleChange}
                    data-testid="phone-input"
                />
                <input 
                    type="text" 
                    placeholder="country"
                    value={state.country}
                    name="country"
                    onChange={handleChange}
                    data-testid="country-input"
                />
            </section>
            <footer className="modal-footer">
                <button onClick={cleanModal} className="error">close</button>
                <button onClick={onSubmit} data-testid="submit-button">submit</button>
            </footer>
        </article>
    </div>
    )
}

export default RegisterModal;