import './sidebar.css';

const Sidebar = ({ openModal }) => {
    return (
        <div className="sidebar">
            <div className="pseudo sidebar-button">
                <button className="pseudo button">home</button>
            </div>
            <div className="pseudo sidebar-button">
                <button 
                    onClick={openModal} 
                    className="pseudo button" 
                    data-testid="register-button"
                >
                    register
                </button>
            </div>
        </div>
    )
}

export default Sidebar;