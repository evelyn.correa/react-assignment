import './table.css';
import { useState } from 'react';

const Table = ({ data, update, remove }) => {
    const initialState = {
        name: "",
        profession: "",
        email: "",
        phone: "",
        country: ""
    }

    const [newInfo, setNewInfo] = useState(initialState);
    const [edit, setEdit] = useState({editing: false, id: -1});

    const toggleEdit = person => {
        setNewInfo(person);
        setEdit({editing: true, id: person.id});
    }

    const handleChange = e => {
            const { name, value } = e.target;
            setNewInfo(prevState => ({ ...prevState, [name]: value }));
    }

    const renderEditableLine = person => {
        return (
            <tr key={person.id}>
                <td>
                    <input 
                        placeholder="nome"
                        value={newInfo.name}
                        name="name"
                        onChange={handleChange}
                        data-testid="name-field"
                    />
                </td>
                <td>
                    <input 
                        type="text" 
                        placeholder="profession"
                        value={newInfo.profession}
                        name="profession"
                        onChange={handleChange}
                    />
                </td>
                <td>
                    <input 
                        type="email" 
                        placeholder="e-mail"
                        value={newInfo.email}
                        name="email"
                        onChange={handleChange}
                    />
                </td>
                <td>
                    <input 
                        type="tel" 
                        placeholder="phone number"
                        value={newInfo.phone}
                        name="phone"
                        onChange={handleChange}
                    />
                </td>
                <td>
                    <input 
                        type="text" 
                        placeholder="country"
                        value={newInfo.country}
                        name="country"
                        onChange={handleChange}
                    />
                </td>
                <td className="action-buttons">
                    <button 
                        onClick={() => {
                            setEdit({ editing: false, id: -1 })
                            update(person.id, newInfo)
                        }}
                        data-testid="update-button"
                    >
                        save
                    </button>
                    <button 
                        className="error" 
                        onClick={() => setEdit({ editing: false, id: -1 })}
                    >
                        cancel
                    </button>
                </td>
            </tr>
        )
    }

    const renderLine = person => {
        return (
            edit.editing && edit.id === person.id ? 
                renderEditableLine(person)
            : <tr key={person.id}>
                <td>{person.name}</td>
                <td>{person.profession}</td>
                <td>{person.email}</td>
                <td>{person.phone}</td>
                <td>{person.country}</td>
                <td className="action-buttons">
                    <button
                        onClick={() => toggleEdit(person)}
                        data-testid="toggle-edit-button"
                    >
                        update
                    </button>
                    <button
                        className="error" 
                        onClick={() => remove(person.id)}
                        data-testid="remove-button"
                    >
                        delete
                    </button>
                </td>
            </tr>)
    }

    return (
    <table className="primary table">
        <thead>
            <tr>
                <th>name</th>
                <th>profession</th>
                <th>e-mail</th>
                <th>phone number</th>
                <th>country</th>
                <th>actions</th>
            </tr>
        </thead>
        <tbody>
               {data.length &&
                data.map(person => renderLine(person))}
        </tbody>
    </table>)
}

export default Table;
